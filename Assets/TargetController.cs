using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetController : MonoBehaviour
{
    [SerializeField] private MeshRenderer renderer;
    [SerializeField] private ThirdPersonShooterController _shooterController;
    [SerializeField] private TargetType type;
    [SerializeField] private bool active;
    [SerializeField] private bool isOnCooldown;
    private float timer;


    private void FixedUpdate()
    {
        timer -= Time.deltaTime;
        if (timer < 0)
        {
            isOnCooldown = false;
        }
        else
        {
            isOnCooldown = true;
        }
    }

    private void OnHit()
    {
        timer = 1;
        active = !active;
        if (active)
        {
            renderer.material.color = Color.green;
        }
        else
        {
            renderer.material.color = Color.grey;
        }

        switch (type)
        {
            case TargetType.RedBulletSelector:
                _shooterController.redBullet = active;
                break;
            case TargetType.BigBulletSelector: 
                _shooterController.bigBullet = active;
                break;
            case TargetType.BombBulletSelector: 
                _shooterController.bombBullet = active;
                break;

        }
    }
    
    private void OnTriggerExit(Collider col)
    {
        if (col.CompareTag("Bullet") && !isOnCooldown)
        {
           OnHit();
        }
    }
}

public enum TargetType
{
    RedBulletSelector,
    BigBulletSelector,
    BombBulletSelector
}
