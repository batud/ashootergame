using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    [SerializeField] private Transform hitVfx;
    [SerializeField] private MeshRenderer bulletRenderer;
    private bool bombBullet;
    private Vector3 targetPosition;

    public void SetTarget(Vector3 targetPosition)
    {
        this.targetPosition = targetPosition;
    }

    public void SetBulletType(bool isRed, bool isBig, bool isBomb)
    {
        if (isRed)
        {
            bulletRenderer.material.color = Color.red;
        }
        else
        {
            bulletRenderer.material.color = Color.white;
        }
        if (isBig)
        {
            transform.localScale = Vector3.one*2;
        }
        else
        {
            transform.localScale = Vector3.one;
        }
        if (isBomb)
        {
            bombBullet = true;
        }
    }

    private void Explode()
    {
        Instantiate(hitVfx, targetPosition, quaternion.identity);
    }

    private void FixedUpdate()
    {
        float distanceBefore = Vector3.Distance(transform.position, targetPosition);
        Vector3 movedir = (targetPosition - transform.position).normalized;
        float moveSpeed = 20;
        transform.position += movedir * moveSpeed * Time.deltaTime;
        transform.GetChild(0).LookAt(targetPosition);
        float distanceAfter = Vector3.Distance(transform.position, targetPosition);
        if (distanceBefore<distanceAfter)
        {
            if (bombBullet)
            {
                Explode();
            }
            Destroy(gameObject);
        }
    }
}
