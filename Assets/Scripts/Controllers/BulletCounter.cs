using System;
using System.Collections;
using System.Collections.Generic;
using StarterAssets;
using TMPro;
using UnityEngine;

public class BulletCounter : MonoBehaviour
{
    [SerializeField]private TextMeshPro counterText;
    private int counter;
    
    public void ShotsFired()
    {
        counter++;
        counterText.text = counter.ToString();
    }
}
