using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using StarterAssets;
using TMPro;

public class ThirdPersonShooterController : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera aimVirtualCamera;
    [SerializeField] private float normalSensitivity;
    [SerializeField] private float aimSensitivity;
    [SerializeField] private LayerMask aimColliderLayerMask = new LayerMask();
    [SerializeField] private Transform aimTransform;
    [SerializeField] private Transform pfBulletProjectile;
    [SerializeField] private Transform bulletSpawn;
    [SerializeField] private int shotgunBulletCount;
    [SerializeField] private int shotgunSpread;
    [SerializeField] private BulletCounter bulletCounter;
    
    private ThirdPersonController thirdPersonController;
    private StarterAssetsInputs starterAssetsInputs;
    private Animator animator;
    private Vector3 mouseWorldPosition = Vector3.zero;
    private Vector2 screenCenterPoint = new Vector2(Screen.width / 2f, Screen.height / 2f);

    public bool redBullet;
    public bool bigBullet;
    public bool bombBullet;



    private void Awake()
    {
        thirdPersonController = GetComponent<ThirdPersonController>();
        starterAssetsInputs = GetComponent<StarterAssetsInputs>();
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        AimHandler();
        ShootHandler();
    }

    private void AimHandler()
    {
        Ray ray = Camera.main.ScreenPointToRay(screenCenterPoint);
        if (Physics.Raycast(ray, out RaycastHit raycastHit, 999f, aimColliderLayerMask))
        {
            aimTransform.position = raycastHit.point;
            mouseWorldPosition = raycastHit.point;
        }

        if (starterAssetsInputs.aim)
        {
            aimVirtualCamera.gameObject.SetActive(true);
            thirdPersonController.SetSensitivity(aimSensitivity);
            thirdPersonController.SetRotateOnMove(false);
            animator.SetLayerWeight(1, Mathf.Lerp(animator.GetLayerWeight(1), 1f, Time.deltaTime * 13f));

            Vector3 worldAimTarget = mouseWorldPosition;
            worldAimTarget.y = transform.position.y;
            Vector3 aimDirection = (worldAimTarget - transform.position).normalized;

            transform.forward = Vector3.Lerp(transform.forward, aimDirection, Time.deltaTime * 20f);
        }
        else
        {
            aimVirtualCamera.gameObject.SetActive(false);
            thirdPersonController.SetSensitivity(normalSensitivity);
            thirdPersonController.SetRotateOnMove(true);
            animator.SetLayerWeight(1, Mathf.Lerp(animator.GetLayerWeight(1), 0f, Time.deltaTime * 13f));
        }
    }

    private void ShootHandler()
    {
        if (starterAssetsInputs.shoot)
        {
            for (int i = 0; i < shotgunBulletCount; i++)
            {
                var bullet = Instantiate(pfBulletProjectile);
                bullet.position = bulletSpawn.position;
                var bulletController =bullet.GetComponent<BulletController>();
                bulletController.SetTarget(aimTransform.position + new Vector3(Random.Range(0,shotgunSpread)/100f,Random.Range(0,shotgunSpread)/100f,0));
                bulletController.SetBulletType(redBullet,bigBullet,bombBullet);
                bulletCounter.ShotsFired();
                starterAssetsInputs.shoot = false;
            }
        }
    }
}