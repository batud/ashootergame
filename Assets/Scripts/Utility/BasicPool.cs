using System.Collections.Generic;
using UnityEngine;

    public class BasicPool<T>
        where T : Component
    {
        public T Prefab { get; private set; }
        public List<T> Objects { get; private set; }
        private Transform Parent;

        public T GetInstance()
        {
            for (int i = 0; i < Objects.Count; i++)
            {
                if (!Objects[i].gameObject.activeSelf)
                {
                    Objects[i].gameObject.SetActive(true);
                    return Objects[i];
                }
            }

            Debug.LogError("There is none object left in the pool " + Prefab.name);
            return null;
        }

        public void BackToPool(GameObject gameObject)
        {
            gameObject.SetActive(false);
        }

        public BasicPool(T prefab, Transform parent, int capacity)
        {
            Prefab = prefab;
            Parent = parent;
            Objects = new List<T>();
            AddCapacity(capacity);
        }

        public void AddCapacity(int capacity)
        {
            for (int i = 0; i < capacity; i++)
            {
                T t = GameObject.Instantiate(Prefab, Parent);
                Objects.Add(t);
                t.gameObject.SetActive(false);
            }
        }
    }

    public class BasicPool
    {
        public GameObject Prefab
        {
            get; private set;
        }
        public List<GameObject> Objects
        {
            get; private set;
        }
        private Transform Parent;

        public GameObject GetInstance()
        {
            for (int i = 0; i < Objects.Count; i++)
            {
                if (!Objects[i].gameObject.activeSelf)
                {
                    Objects[i].gameObject.SetActive(true);
                    return Objects[i];
                }
            }

            Debug.LogError("There is none object left in the pool " + Prefab.name + " |  Increasing PoolSize From :" + Objects.Count + " to :" + (Objects.Count + 10));
            AddCapacity(10);

            for (int i = 0; i < Objects.Count; i++)
            {
                if (!Objects[i].gameObject.activeSelf)
                {
                    Objects[i].gameObject.SetActive(true);
                    return Objects[i];
                }
            }

            return null;
        }
        public void AddCapacity(int capacity)
        {
            for (int i = 0; i < capacity; i++)
            {
                GameObject t = GameObject.Instantiate(Prefab, Parent);
                Objects.Add(t);
                t.gameObject.SetActive(false);
            }
        }

        public void BackToPool(GameObject gameObject)
        {
            gameObject.SetActive(false);
        }

        public BasicPool(GameObject prefab, Transform parent, int capacity)
        {
            Prefab = prefab;
            Parent = parent;
            Objects = new List<GameObject>();
            AddCapacity(capacity);
        }
    }

