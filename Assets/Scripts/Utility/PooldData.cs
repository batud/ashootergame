﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

    [System.Serializable]
    public class Entry
    {
        public GameObject prefab;
        private BasicPool basicPool;
        public int poolSize;

        public void Initialize(Transform parent)
        {
            basicPool = new BasicPool(prefab, parent, poolSize);
        }

        public GameObject GetInstance()
        {
            return basicPool.GetInstance();
        }
    }


[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/PoolData", order = 1)]
    public class PooldData :  SingletonScriptableObject<PooldData>
    {
      
        public Entry[] objectEntries = null;

        public void Initialize(Transform parent)
        {
            for (int i = 0; i < objectEntries.Length; i++)
            {
                objectEntries[i].Initialize(parent);
            }
        }
    }